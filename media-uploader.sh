#!/bin/bash

SSH_KEY="/home/chris/.ssh/id_rsa"
LOCAL_PATH="/media/john.qnap/Plex/"

REMOTE_HOST="10.9.0.18"
REMOTE_PATH="/share/Plex/"


# Sit behind a file lock. Only one at a time, please!
# Min depth is 2 to keep Plex/{dirA,dirB,etc} but delete deeper nested dirs
/usr/bin/flock -w 0 /tmp/plex-media-uploader.lock \
    /usr/bin/rsync -e "ssh -i "$SSH_KEY \
	--recursive \
        --remove-source-files \
	$LOCAL_PATH $REMOTE_HOST:$REMOTE_PATH \
    && /usr/bin/find $LOCAL_PATH -mindepth 2 -depth -type d -exec rmdir {} \;
